### mulesoft_dev_pipeline

![Image of Yaktocat](https://www.vectorlogo.zone/logos/mulesoft/mulesoft-ar21.svg)

This is common development mulesoft pipeline to *build and deploy*  
[Lexi Runtime CI/CD documentation](https://jiraims.rm.imshealth.com/wiki/pages/viewpage.action?pageId=191614779)  
  
Here are some example projects:
- Source code for API:              https://gitlabrnds-dev.quintiles.com/lexi_group/lexiclinical/iqvia-rds-gsk-mcoding-sys/tree/master
- Deployment properties:            https://gitlabrnds-dev.quintiles.com/lexi_group/rds-artifacts/blob/master/bundle.json


