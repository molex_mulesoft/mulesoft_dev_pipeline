#!/bin/bash
#------------------------------------------------------------------------------
# Author(s)    : DevOps Team --- 09/12/2019
#------------------------------------------------------------------------------
# Copyright (c) iqvia
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
# 
###############################################################################

###############################################################################
# build.sh
###############################################################################
# Description  : To launch mulesoft API build process
#
################################################################################
#TODO:
#
#
################################################################################

#####################################
#         Variables                 #
#####################################
ROOT_PATH=$(pwd)

#####################################
#         Fonctions                 #
#####################################
packages()
{
echo "deb http://deb.debian.org/debian/ stretch main" > /etc/apt/sources.list
apt-get update
#apt-get install libxml2-utils xmlstarlet -y
apt-get install libxml2-utils xmlstarlet -y --allow-unauthenticated
}

executetest()
{
echo "Launch build"
pwd
echo ${SOURCEPROJECTPATH}
echo ${ENV}

#eval $(ssh-agent -s)
#ssh-add ~/.ssh/id_rsa_lexi
#echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
#CMD="git clone ssh://git@gitlabrnds-dev.quintiles.com:mulesoft_grp/${SOURCEPROJECTPATH}.git"
CMD="git clone https://${GIT_Y_USERNAME}:${GIT_Y_PASSWORD}@${GITLAB_HOST}/${SOURCEPROJECTPATH}.git"
bash check_git_clone.sh "${CMD}" 2>/dev/null
#ls -lsa .
cd ${PROJECT}
git checkout ${CHECKOUT}

mvn $MAVEN_CLI_OPTS clean test -Dpackaging=${FILE_TYPE} -DskipMunitTests=false -Dhttps.port=8443 -Dmule.env=dev -Dmule.key=1234567890123456

if [ "${FILE_TYPE}" == "zip" ]; then
  if [ ! -z "${SOURCETAG}" ]; then xmlstarlet ed -N x="http://maven.apache.org/POM/4.0.0" -u "/x:project/x:version" -v "${SOURCETAG}" pom.xml > pom.xml.new; cp -f pom.xml.new pom.xml; fi  
fi

#NEXUS_URL=$(xmllint --xpath "/*[local-name() = 'settings']/*[local-name() = 'profiles']/*[local-name() = 'profile']/*[local-name() = 'repositories']/*[local-name() = 'repository']/*[local-name() = 'url']/text()" .m2/settings.xml | sed -e 's/iqvia-group/iqvia-internal/g')
NEXUS_URL="https://${NEXUS_HOST}/repository/snapshots/"
echo "NEXUS_URL=${NEXUS_URL} -- ${FILE_TYPE}"
mvn $MAVEN_CLI_OPTS package -Dpackaging=${FILE_TYPE} -DskipMunitTests -Dhttps.port=8443 -Dmule.env=dev -Dmule.key=1234567890123456

if [ "${FILE_TYPE}" == "jar" ]; then
  echo "move ********* ${CI_PROJECT_DIR}/${PROJECT}/target/${PROJECT}-*.jar ${CI_PROJECT_DIR}/${PROJECT}/target/${APPLICATION_FILE}"
  mv ${CI_PROJECT_DIR}/${PROJECT}/target/${PROJECT}-*.jar ${CI_PROJECT_DIR}/${PROJECT}/target/${APPLICATION_FILE}
  echo "moved the file"
fi

echo "APPLICATION_FILE=${APPLICATION_FILE}"
echo "Deploying now........................."
mvn $MAVEN_CLI_OPTS deploy -DskipMunitTests -Dpackaging=${FILE_TYPE} -Dversion=${FILE_VER} -DpomFile=pom.xml -DgeneratePom=false -DrepositoryId=snapshots -Durl="${NEXUS_URL}" -Dfile="${CI_PROJECT_DIR}/${PROJECT}/target/${APPLICATION_FILE}"



}
############################
#       MAIN               #
############################
packages
executetest