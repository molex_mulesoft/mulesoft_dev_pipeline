#!/bin/bash
#------------------------------------------------------------------------------
# Author(s)    : DevOps Team --- 09/12/2019
#------------------------------------------------------------------------------
# Copyright (c) iqvia
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
# 
###############################################################################

###############################################################################
# deploy.sh
###############################################################################
# Description  : To launch deploy mulesoft runtime process
#
################################################################################
#TODO:
#
#
################################################################################

#####################################
#         Variables                 #
#####################################
ROOT_PATH=$(pwd)

#####################################
#         Fonctions                 #
#####################################
get_tools()
{
echo "Get tools/deploy script"
GITLABGROUP=$(echo ${SOURCEPROJECTPATH} | awk -F"/" '{print $1}')
echo "GITLABGROUP=${GITLABGROUP}"
#CMD="git clone ssh://git@gitlab.ims.io:2222/techsol-devops/mulesoft_ipaas/tools/deploy-script.git"
CMD="git clone https://${GIT_Y_USERNAME}:${GIT_Y_PASSWORD}@${GITLAB_HOST}/${GITLABGROUP}/deploy-script-master.git"
bash check_git_clone.sh "${CMD}" 2>/dev/null
cd deploy-script-master
git checkout ${CHECKOUT}
cd ..
if [ ! -f "deploy-script-master/deploy.sh" ]; then echo "The deploy.sh script is not present !"; exit -1;fi
}

get_envfiles()
{
echo "Get env_files/environment credentials files"
#CMD="git clone ssh://git@gitlab.ims.io:2222/${GITLABGROUP}/${PRODUCT}/env-files.git"
CMD="git clone -b master https://${GIT_Y_USERNAME}:${GIT_Y_PASSWORD}@${GITLAB_HOST}/${GITLABGROUP}/env-files.git"
bash check_git_clone.sh "${CMD}" 2>/dev/null
cd env-files
#if git checkout ${CHECKOUT}; then echo -e "\nCheckout to the ${CHECKOUT} ${CHECKOUTTYPE} in the env-files project OK\n"; else echo -e "\nThe ${CHECKOUT} ${CHECKOUTTYPE} doesn't exist in the env-files project\n";exit -1;fi
git checkout ${CHECKOUT}}
cd ..
if [ ! -f "env-files/environments.json" ]; then echo "The environments.json file is not present !"; exit -1;fi
if [ ! -f "env-files/credentials" ]; then echo "The anypoint-cli credentials file is not present !"; exit -1;fi
}

get_artifacts()
{
echo "Get artifacts/bundle files"
echo ${GITLABGROUP}
echo ${PRODUCT}
#CMD="git clone ssh://git@gitlab.ims.io:2222/${GITLABGROUP}/${PRODUCT}/ts-artifacts.git"
CMD="git clone https://${GIT_Y_USERNAME}:${GIT_Y_PASSWORD}@${GITLAB_HOST}/${GITLABGROUP}/rds-artifacts.git"
bash check_git_clone.sh "${CMD}" 2>/dev/null
cd rds-artifacts
#if git checkout ${CHECKOUT}; then echo -e "\nCheckout to the ${CHECKOUT} ${CHECKOUTTYPE} in the rds-artifacts project OK\n"; else echo -e "\nThe ${CHECKOUT} ${CHECKOUTTYPE} doesn't exist in the rds-artifacts project\n";exit -1;fi
git checkout ${CHECKOUT}
cd ..
if [ ! -f "rds-artifacts/bundle.json" ]; then echo "The bundle.json file is not present !"; exit -1;fi 
}

deploy()
{
echo "Launch deploy"
pwd
echo ${ENV}

cp env-files/environments.json deploy-script-master/
mkdir ~/.anypoint/
cp -r env-files/credentials ~/.anypoint/
cp rds-artifacts/bundle.json deploy-script-master/
mkdir deploy-script-master/runtime-mgr/
cp ${CI_PROJECT_DIR}/${PROJECT}/target/${APPLICATION_FILE} deploy-script-master/runtime-mgr/

ls -lsa .
ls -lsa deploy-script-master/runtime-mgr/
ls -lsa deploy-script-master/
ls -lsa rds-artifacts
ls -lsa env-files

echo ${PROJECT}
echo ${ENV}

URLSTRING=$(jq '."'${PROJECT}'.dev.properties".urlString' deploy-script-master/bundle.json | sed -e 's/"//g')
 if [[ "${URLSTRING}" == "null" ]];then echo "URLSTRING is not defined, so exit !"; exit -1; else echo "URLSTRING: ${URLSTRING}"; fi

ENVIRONMENT=$(jq '."'${ENV}'"' deploy-script-master/environments.json)
 if [[ "${ENVIRONMENT}" == "null" ]];then echo "ENVIRONMENT is not defined, so exit !"; exit -1; else echo "ENVIRONMENT: ${ENVIRONMENT}"; fi

URLVERSION=$(jq '."'${PROJECT}'.dev.properties".urlVersion' deploy-script-master/bundle.json | sed -e 's/"//g')
 if [[ "${URLVERSION}" == "null" ]];then echo "URLVERSION is not defined, so exit !"; exit -1; else echo "URLVERSION: ${URLVERSION}"; fi

URLREGION=$(jq '."'${ENV}'".urlRegion' deploy-script-master/environments.json | sed -e 's/"//g')
 if [[ "${URLREGION}" == "null" ]];then echo "URLREGION is not defined, so exit !"; exit -1; else echo "URLREGION: ${URLREGION}"; fi

URLENVIRONMENT=$(jq '."'${ENV}'".urlEnvironment' deploy-script-master/environments.json | sed -e 's/"//g')
 if [[ "${URLENVIRONMENT}" == "null" ]];then echo "URLENVIRONMENT is not defined, so exit !"; exit -1; else echo "URLENVIRONMENT: ${URLENVIRONMENT}"; fi

anypointProfile=$(jq -r ".\"$URLENVIRONMENT\".anypointCliProfile" deploy-script-master/environments.json)
#echo "profile name $anypointProfile"

cd env-files
if [ -n "${ch_username}" ] && [ -n "${ch_password}" ]; then
    jq --arg new "$ch_username" '(."'$anypointProfile'"."username") |= $new' credentials > tmp.json && mv tmp.json credentials
    jq --arg new "$ch_password" '(."'$anypointProfile'"."password") |= $new' credentials > tmp.json && mv tmp.json credentials
fi
#cat credentials
cd ..

cp -r env-files/credentials ~/.anypoint/

APPLICATION_NAME="${COMPANY}-${PREFIXPRODUCT}-${URLSTRING}-${URLVERSION}-${URLENVIRONMENT}"
 if [ "${#APPLICATION_NAME}" -gt "42" ]; then echo "APPLICATION_NAME ${APPLICATION_NAME} length greater than 42 chars, actual size ${#APPLICATION_NAME}"; exit -1 ; else echo "actual size of APPLICATION_NAME ${APPLICATION_NAME} ${#APPLICATION_NAME}" ; fi

echo "APPLICATION_NAME=${APPLICATION_NAME}"
echo "APPLICATION_FILE=${APPLICATION_FILE}"
cd deploy-script-master
bash -x deploy.sh -e "${ENV}" ${APPLICATION_FILE}
}

############################
#       MAIN               #
############################
get_tools
get_envfiles
get_artifacts
deploy