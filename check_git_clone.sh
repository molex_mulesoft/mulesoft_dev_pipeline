#!/bin/bash
#------------------------------------------------------------------------------
# Author(s)    : Devops Team --- 08/29/19
#------------------------------------------------------------------------------
# Copyright (c) iqvia
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
###############################################################################

###############################################################################
# check_git_clone.sh
###############################################################################
# Description  : 
# Check the git clone command
# If the command fails then the project older is removed and the clone is rerun
###############################################################################

#####################################
#         Arguments                 #
#####################################
#CMD: git clone command
#####################################

#####################################
#         Main                      #
#####################################
CMD=$1
PROJECT_NAME=$(echo $CMD | awk -F".git" '{print $(NF-1)}' | awk -F"/" '{print $NF}')
${CMD}
CR=$?
if [ ${CR} -eq 0 ];then 
    echo "The ${PROJECT_NAME} project has been cloned with success" 
else 
    rm -rf ${PROJECT_NAME}
    ${CMD}
    echo "The ${PROJECT_NAME} folder has been removed and the ${PROJECT_NAME} project has been cloned with success"
fi